import styled from 'styled-components'

const Div = styled.div`
  color: #fff;
  border: 1px solid #000;
  background: 10% 10%;
  background-color: #3b3c3d;
  height: 200px;
`

const Navbar = () => {
    const love = "<3";
    return (
      <Div>
        <div className="headerTitle">
          <h1 className='titleHeader'>HN Feed</h1>
          <h3 className='subtitle'>We {love} Hacker News</h3>
        </div>
      </Div>
    )
}

export default Navbar;