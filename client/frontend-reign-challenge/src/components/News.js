import { useEffect, useState } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { library } from '@fortawesome/fontawesome-svg-core'

library.add(faTrashAlt)

const Div = styled.div`
  padding: 10px;
  margin: 15px;
`
const useGetNews = () => {
  const [news, setNews] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    window
      .fetch('http://172.16.238.11:5000/news')
      .then((res) => res.json())
      .then((response) => {
        setNews(response)
        setLoading(false)
      })
  }, [])

  const deleteProducto = () => {

    fetch(`http://172.16.238.11:5000/news/` , {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        isDeleted: true,
      }),
    })
      .then((res) => res.json())
      .then((result) => result)
      .catch((err) => console.log({ msg: 'An error has ocurred', error: err }))

    const refreshPage = () => {
      window.location.reload(true)
    }

    setTimeout(refreshPage(), 10000)
  }

  return { news, loading, deleteProducto }
}



const News = () => {

   const { news, loading, deleteProducto } = useGetNews()
   const newsFetched = news;
   
   const renderNews = () => (
     <Div>
       {loading ? (
         <div key='loading'>Loading...</div>
       ) : (
         newsFetched.map((news) =>
           news.story_title === null ||
           (news.title === true && news.isDeleted === true) ? (
             <div key={news._id} className='hideRow' />
           ) : (
             <div key={news._id} className='row'>
               <div className='column titleCol'>
                 <div>
                   <a
                     className='links'
                     href={`${news.story_url}`}
                     target='_blank'
                     rel='noreferrer'
                   >
                     <span className='title'>{news.story_title}</span>
                   </a>
                   <span className='author'>- {news.author} -</span>
                 </div>
               </div>
               <div className='column timeCol'>
                 <div>
                   <span className='time'>
                     {moment(news.created_at).format('L')}
                   </span>
                 </div>
               </div>
               <div className='column iconCol'>
                 <div>
                   <span>
                     <a
                       href='/'
                       role='button'
                       onClick={deleteProducto}
                       rel='noreferrer'
                     >
                       <FontAwesomeIcon icon='trash-alt' />
                     </a>
                   </span>
                 </div>
               </div>
             </div>
           )
         )
       )}
     </Div>
   )

    return (

      <Div>
        {renderNews()}
      </Div>
    )
}

export default News;
