import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  *, *:before, *:after {
     margin: 0;
  box-sizing: border-box;
  }
  body {
      font-family: 'Balsamiq Sans', cursive;
  }

  .row {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;
    background-color: #fff;
    border-bottom: 1px solid #ccc;
  }

.column {
    display: flex;
    flex-direction: column;
    flex-basis: 50%;
    height: 30px;
    padding: 5px;
    align-items: flex-start;
    justify-content: center;
}
.doubleColumn {
    display: flex;
    flex-direction: column;
    flex-basis: 100%;
}

.title {
    color: #333;
    font-size: 13px;
}
.time {
    color: #333;
    font-size: 13px;
    margin-left: auto;
    margin-right: auto;
}
.author {
    color: #999;
    font-size: 13px;
}
.titleHeader {
    font-size: 50px;
}
.headerTitle {
  padding: 20px;
  margin: 20px;
}

.timeCol {
  flex: 0!important;
  flex-basis: 8%!important;
}

.titleCol {
  flex: 0!important;
  flex-basis: 85%!important;
}

.iconCol {
  flex: 0!important;
  flex-basis: 1%!important;
}

.row:hover {
  background-color: #fafafa;
}

.links {
  text-decoration: unset;
}

.hideRow {
  display: none;
}
@media screen and (min-width: 800px) {

  .author {
      margin-left: .5em;
  }
}
`
export default GlobalStyle;