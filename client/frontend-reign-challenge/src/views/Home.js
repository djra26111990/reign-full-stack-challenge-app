import Header from '../components/Header'
import News from '../components/News'

const Home = () => {
  return (
    <>
      <Header />
      <News />
    </>
  )
}

export default Home;
