# REIGN FULL STACK APP CHALLENGE

This project was created using nestjs has a nodejs framework using typescript as language. On the client side, the frontend was created using create-react-app using VainillaJS.

The backend and frontend was dockerized, having their own Dockerfile document inside each folder. 

In the Root folder was found the docker-compose.yml with all configs needed to run the full app.

## It's needed have Docker installed on the computer and the package docker-compose to run this command.

### `docker-compose up` to run and build the images (in the first run) and created the containers.

### `docker-compose down` to stop the containers.

## The Url to the frontend is: http://172.16.238.10:3000/




