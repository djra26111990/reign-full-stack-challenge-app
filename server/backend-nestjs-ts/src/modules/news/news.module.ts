import { Module, HttpModule } from '@nestjs/common';
import { NewsService } from '../../services/news/news.service';
import { NewsController } from '../../controllers/news/news.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsSchema, News } from '../../models/newsModel.model';
import { AppService } from '../../app.service';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([{ name: News.name, schema: NewsSchema }]),
  ],
  providers: [AppService, NewsService],
  controllers: [NewsController],
})
export class NewsModule {}
