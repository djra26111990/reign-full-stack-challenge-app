import { Schema, Prop, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class News extends Document {
  @Prop({ require: true })
  created_at: string;

  @Prop({ type: String, default: null })
  title: string;

  @Prop({ type: String, default: null })
  url: string;

  @Prop()
  author: string;

  @Prop({ type: Number, default: null })
  points: number;

  @Prop({ type: String, default: null })
  story_text: string;

  @Prop({ type: String, default: null })
  comment_text: string;

  @Prop({ type: Number, default: null })
  num_comments: number;

  @Prop()
  story_id: number;

  @Prop({ type: String, default: null })
  story_title: string;

  @Prop({ type: String, default: null })
  story_url: string;

  @Prop()
  parent_id: number;

  @Prop()
  created_at_i: number;

  @Prop([String])
  _tags: string[];

  @Prop()
  objectID: string;

  @Prop(
    raw({
      author: { value: String, matchLevel: String, matchedWords: [] },
      comment_text: {
        value: String,
        matchLevel: String,
        fullyHighlighted: Boolean,
        matchedWords: [],
      },
      story_title: { value: String, matchLevel: String, matchedWords: [] },
      story_url: { value: String, matchLevel: String, matchedWords: [] },
    }),
  )
  _highlightResult: Record<string, any>;

  @Prop({ type: Boolean, default: false })
  isDeleted: boolean;
}

export const NewsSchema = SchemaFactory.createForClass(News);
