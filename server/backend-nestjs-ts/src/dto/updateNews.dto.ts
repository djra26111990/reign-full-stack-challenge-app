import { PartialType } from '@nestjs/mapped-types';
import { IsBoolean } from 'class-validator';
import { CreateNewsDto } from './news.dto';

export class UpdateNewsDto extends PartialType(CreateNewsDto) {
  @IsBoolean()
  readonly isDeleted;
}
