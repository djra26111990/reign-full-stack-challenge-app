import { IsString, IsArray, IsNumber, IsDate, IsObject } from 'class-validator';

export class CreateNewsDto {
  @IsDate()
  readonly created_at: string;

  @IsString()
  readonly title: string;

  @IsString()
  readonly url: string;

  @IsString()
  readonly author: string;

  @IsNumber()
  readonly points: number;

  @IsString()
  readonly story_text: string;

  @IsString()
  readonly comment_text: string;

  @IsNumber()
  readonly num_comments: number;

  @IsNumber()
  readonly story_id: number;

  @IsString()
  readonly story_title: string;

  @IsString()
  readonly story_url: string;

  @IsNumber()
  readonly parent_id: number;

  @IsNumber()
  readonly created_at_i: number;

  @IsArray()
  readonly _tags: string[];

  @IsNumber()
  readonly objectID: string;

  @IsObject()
  readonly _highlightResult: Record<string, any>;
}
