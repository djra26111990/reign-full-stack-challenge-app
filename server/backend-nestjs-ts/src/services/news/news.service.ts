import {
  Injectable,
  HttpService,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { CreateNewsDto } from 'src/dto/news.dto';
import { UpdateNewsDto } from 'src/dto/updateNews.dto';
import { INews } from 'src/interfaces/news.interface';
import { News } from '../../models/newsModel.model';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class NewsService {
  constructor(
    private httpService: HttpService,
    @InjectModel(News.name) private readonly newsModel: Model<News>,
  ) {}
  private readonly logger = new Logger(NewsService.name);

  @Cron(new Date(Date.now() + 20 * 1000))
  public async serverStarted() {
    try {
      this.logger.debug(`data fetched at server started`);
      const newsFetchedToSave: any[] = [...(await this.getNews())];
      const options: Record<string, any> = {
        headers: { 'Content-Type': 'application/json' },
      };

      newsFetchedToSave.map((elem) =>
        this.httpService
          .post('http://localhost:5000/news', elem, options)
          .toPromise(),
      );
    } catch (err) {
      return console.log({
        msg: 'Has ocurred an error',
        error: err,
      });
    }
  }

  @Cron(CronExpression.EVERY_HOUR)
  public async updateDb() {
    try {
      this.logger.debug(`data updated in db at this moment`);
      const newsFetchedToSave: any[] = [...(await this.getNews())];
      const options: Record<string, any> = {
        headers: { 'Content-Type': 'application/json' },
      };

      newsFetchedToSave.map((elem) =>
        this.httpService
          .post('http://localhost:5000/news', elem, options)
          .toPromise(),
      );
    } catch (err) {
      return console.log({
        msg: 'Has ocurred an error',
        error: err,
      });
    }
  }

  public async getNews() {
    const newsFetched = await this.httpService
      .get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
    return newsFetched.data.hits;
  }

  public async createNews(createNewsDto: CreateNewsDto): Promise<any> {
    const newNews = await new this.newsModel(createNewsDto);
    return newNews.save();
  }

  public async getLocalNews(): Promise<INews[]> {
    return await this.newsModel.find().sort({ created_at: -1 }).exec();
  }

  public async deleteNews(
    newsId: string,
    updateNewsDto: UpdateNewsDto,
  ): Promise<any> {
    const existingNews = await this.newsModel.findByIdAndUpdate(
      { _id: newsId },
      updateNewsDto,
      { new: true },
    );

    if (!existingNews) {
      throw new NotFoundException(`Post with ID#${newsId} was not found`);
    }

    return existingNews;
  }
}
