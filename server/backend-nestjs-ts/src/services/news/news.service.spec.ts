import { HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { News, NewsSchema } from '../../models/newsModel.model';
import { NewsService } from '../../services/news/news.service';

describe('test news service', () => {
  let moduleMock: TestingModule;
  let newsService: NewsService;

  beforeAll(async () => {
    moduleMock = await Test.createTestingModule({
      imports: [
        HttpModule,
        MongooseModule.forFeature([{ name: News.name, schema: NewsSchema }]),
        MongooseModule.forRoot(
          'mongodb+srv://formuser:8702297998289aa.@clustertestdevapps0.sgvd1.gcp.mongodb.net/hackernewsfetched?retryWrites=true&w=majority',
          {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
          },
        ),
      ],
      providers: [NewsService],
    }).compile();
    newsService = moduleMock.get<NewsService>(NewsService);
  });

  it('Service must be defined', async () => {
    expect(newsService).toBeDefined();
  });

  it('should return all data', async () => {
    const data = await newsService.getLocalNews();
    expect.objectContaining(data);
  });
});

describe('test news service fake call', () => {
  let moduleMock: TestingModule;
  let newsService: NewsService;

  const mockService = {
    createNews: jest.fn().mockResolvedValue('test'),
  };

  beforeAll(async () => {
    moduleMock = await Test.createTestingModule({
      providers: [NewsService],
    })
      .overrideProvider(NewsService)
      .useValue(mockService)
      .compile();
    newsService = moduleMock.get<NewsService>(NewsService);
  });

  it('should create a post', async () => {
    const mockDto = {
      created_at: '2021-01-30T19:40:51.000Z',
      title: null,
      url: null,
      author: 'nodesocket',
      points: null,
      story_text: null,
      comment_text:
        'Or how about the big elephant in the room that nobody seems to be bring up; the effect on small businesses that can’t afford to pay employees $15 or $20&#x2F;hr. So, instead of helping the poor, these policies raise unemployment and actually are a detriment to the overall health of the economy.',
      num_comments: null,
      story_id: 25971227,
      story_title:
        'Universal Basic Income Is Superior to a $15 Minimum Wage (2019)',
      story_url:
        'https://basicincometoday.com/opinion-universal-basic-income-is-superior-to-a-15-minimum-wage/',
      parent_id: 25972112,
      created_at_i: 1612035651,
      _tags: ['comment', 'author_nodesocket', 'story_25971227'],
      objectID: '25972238',
      _highlightResult: {
        author: {
          value: '<em>nodes</em>ocket',
          matchLevel: 'full',
          fullyHighlighted: false,
          matchedWords: ['nodejs'],
        },
        comment_text: {
          value:
            'Or how about the big elephant in the room that nobody seems to be bring up; the effect on small businesses that can’t afford to pay employees $15 or $20/hr. So, instead of helping the poor, these policies raise unemployment and actually are a detriment to the overall health of the economy.',
          matchLevel: 'none',
          matchedWords: [],
        },
        story_title: {
          value:
            'Universal Basic Income Is Superior to a $15 Minimum Wage (2019)',
          matchLevel: 'none',
          matchedWords: [],
        },
        story_url: {
          value:
            'https://basicincometoday.com/opinion-universal-basic-income-is-superior-to-a-15-minimum-wage/',
          matchLevel: 'none',
          matchedWords: [],
        },
      },
    };
    const postData = await newsService.createNews(mockDto);

    expect(typeof mockDto).toBe('object');
    expect(typeof postData).toBe('string');
  });
});
