import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(5000, '0.0.0.0');
  console.log(`Server is started at ${await app.getUrl()}`);
}
bootstrap();
