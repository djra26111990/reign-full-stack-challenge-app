import {
  Controller,
  Get,
  Res,
  HttpStatus,
  Post,
  Body,
  NotFoundException,
  Put,
  Param,
} from '@nestjs/common';
import { CreateNewsDto } from '../../dto/news.dto';
import { UpdateNewsDto } from '../../dto/updateNews.dto';
import { INews } from '../../interfaces/news.interface';
import { NewsService } from '../../services/news/news.service';

@Controller('news')
export class NewsController {
  constructor(private newsService: NewsService) {}
  @Get()
  public async getNews(@Res() res): Promise<INews> {
    try {
      const news = await this.newsService.getLocalNews();
      return res.status(HttpStatus.OK).json(news);
    } catch (err) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        message: 'Internal Server Error',
        status: 500,
        error: err,
      });
    }
  }

  @Post()
  public async addNews(
    @Res() res,
    @Body() createNewsDto: CreateNewsDto,
  ): Promise<INews> {
    try {
      const news = await this.newsService.createNews(createNewsDto);
      return res.status(HttpStatus.OK).json({
        message: 'News has been charged successfully',
        news,
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error: News not loaded!',
        status: 400,
        error: err,
      });
    }
  }

  @Put('/:id')
  public async updateNews(
    @Res() res,
    @Param('id') NewsId: string,
    @Body() updateNewsDto: UpdateNewsDto,
  ) {
    try {
      const news = await this.newsService.deleteNews(NewsId, updateNewsDto);
      if (!news) {
        throw new NotFoundException('Post does not exist!');
      }
      return res.status(HttpStatus.OK).json({
        message: 'Post has been successfully deleted',
        news,
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error: Post not deleted!',
        status: 400,
      });
    }
  }
}
