import { HttpService } from '@nestjs/common';
import { News } from 'src/models/newsModel.model';
import { NewsController } from '../../controllers/news/news.controller';
import { NewsService } from '../../services/news/news.service';
import { Model } from 'mongoose';
import { INews } from 'src/interfaces/news.interface';

describe('NewsController', () => {
  let newsController: NewsController;
  let newsService: NewsService;
  let httpService: HttpService;
  let newsModel: Model<News>;

  beforeEach(() => {
    newsService = new NewsService(httpService, newsModel);
    newsController = new NewsController(newsService);
  });

  describe('getLocalNews', () => {
    it('should return an array of objects', async () => {
      let res: Promise<INews>;
      let result: INews[];
      jest
        .spyOn(newsService, 'getLocalNews')
        .mockImplementation(async () => result);

      expect(await newsController.getNews(res)).toBe(result);
    });
  });
});
