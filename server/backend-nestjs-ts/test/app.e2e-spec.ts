import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { CreateNewsDto } from '../src/dto/news.dto';
import { UpdateNewsDto } from '../src/dto/updateNews.dto';
import * as mongoose from 'mongoose';

describe('E2E Tests for /news Endpoint', () => {
  let app: INestApplication;

  beforeEach(async () => {
    jest.setTimeout(30000);
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  beforeAll((done) => {
    done();
  });

  it('should create a post', () => {
    const post: CreateNewsDto = {
      created_at: '2021-01-30T20:01:25.000Z',
      title: null,
      url: null,
      author: 'nodesocket',
      points: null,
      story_text: null,
      comment_text:
        'Most certainly businesses as a response will be forced to raise prices and thus increase inflation and make everyday products more expensive.<p>These studies are obviously politically and progressively motivated (coming from firms such as american progress) so we have to assume a large scale national increase of minimum wage as significant as this would have unintended negative consequences.',
      num_comments: null,
      story_id: 25971227,
      story_title:
        'Universal Basic Income Is Superior to a $15 Minimum Wage (2019)',
      story_url:
        'https://basicincometoday.com/opinion-universal-basic-income-is-superior-to-a-15-minimum-wage/',
      parent_id: 25972315,
      created_at_i: 1612036885,
      _tags: ['comment', 'author_nodesocket', 'story_25971227'],
      objectID: '25972432',
      _highlightResult: {
        author: {
          value: '<em>nodes</em>ocket',
          matchLevel: 'full',
          fullyHighlighted: false,
          matchedWords: ['nodejs'],
        },
        comment_text: {
          value:
            'Most certainly businesses as a response will be forced to raise prices and thus increase inflation and make everyday products more expensive.<p>These studies are obviously politically and progressively motivated (coming from firms such as american progress) so we have to assume a large scale national increase of minimum wage as significant as this would have unintended negative consequences.',
          matchLevel: 'none',
          matchedWords: [],
        },
        story_title: {
          value:
            'Universal Basic Income Is Superior to a $15 Minimum Wage (2019)',
          matchLevel: 'none',
          matchedWords: [],
        },
        story_url: {
          value:
            'https://basicincometoday.com/opinion-universal-basic-income-is-superior-to-a-15-minimum-wage/',
          matchLevel: 'none',
          matchedWords: [],
        },
      },
    };
    return request(app.getHttpServer())
      .post('/news')
      .set('Accept', 'application/json')
      .send(post)
      .expect(HttpStatus.OK);
  });

  afterAll(async (done) => {
    try {
      await mongoose.disconnect(done);
      await app.close();
      done();
    } catch (err) {
      return console.log(err);
    }
  });
});

describe('E2E Tests for /news Endpoint', () => {
  let app: INestApplication;

  beforeEach(async () => {
    jest.setTimeout(30000);
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  beforeAll((done) => {
    done();
  });

  it('should get all posts', () => {
    return request(app.getHttpServer())
      .get('/news')
      .set('Accept', 'application/json')
      .expect(HttpStatus.OK);
  });

  afterAll(async (done) => {
    try {
      await mongoose.disconnect(done);
      await app.close();
      done();
    } catch (err) {
      return console.log(err);
    }
  });
});

describe('E2E Tests for /news Endpoint', () => {
  let app: INestApplication;

  beforeEach(async () => {
    jest.setTimeout(30000);
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  beforeAll((done) => {
    done();
  });

  it('should update a post. Changing isDeleted value to true', () => {
    const post: UpdateNewsDto = {
      isDeleted: true,
    };
    return request(app.getHttpServer())
      .put('/news/6015e40671e6f7892f182b4c')
      .set('Accept', 'application/json')
      .send(post)
      .expect(HttpStatus.OK);
  });

  afterAll(async (done) => {
    try {
      await mongoose.disconnect(done);
      await app.close();
      done();
    } catch (err) {
      return console.log(err);
    }
  });
});
